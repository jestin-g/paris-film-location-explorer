export const environment = {
  production: true,
  filmLocationApiUri:
    'https://opendata.paris.fr/api/records/1.0/search/?dataset=lieux-de-tournage-a-paris&q=&rows=10000&sort=date_debut',
};
