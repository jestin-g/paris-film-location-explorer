import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FilmLocationService {
  constructor(private httpClient: HttpClient) {}

  getFilmLocations() {
    console.log('API called...');
    return this.httpClient.get(environment.filmLocationApiUri);
  }
}
