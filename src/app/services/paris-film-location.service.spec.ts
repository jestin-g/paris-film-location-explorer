import { TestBed } from '@angular/core/testing';

import { ParisFilmLocationService } from './paris-film-location.service';

describe('ParisFilmLocationService', () => {
  let service: ParisFilmLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParisFilmLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
