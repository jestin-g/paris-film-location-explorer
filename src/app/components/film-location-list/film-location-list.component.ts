import { Component, OnInit, Input } from '@angular/core';
import { FilmLocation } from 'src/app/models/FilmLocation';

@Component({
  selector: 'app-film-location-list',
  templateUrl: './film-location-list.component.html',
  styleUrls: ['./film-location-list.component.css'],
})
export class FilmLocationListComponent implements OnInit {
  @Input() filmLocations?: FilmLocation[];

  constructor() {}

  ngOnInit(): void {}
}
