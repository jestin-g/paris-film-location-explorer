import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmLocationItemComponent } from './film-location-item.component';

describe('FilmLocationItemComponent', () => {
  let component: FilmLocationItemComponent;
  let fixture: ComponentFixture<FilmLocationItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilmLocationItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FilmLocationItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
