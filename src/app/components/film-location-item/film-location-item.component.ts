import { Component, OnInit, Input } from '@angular/core';
import { FilmLocation } from 'src/app/models/FilmLocation';

@Component({
  selector: 'app-film-location-item',
  templateUrl: './film-location-item.component.html',
  styleUrls: ['./film-location-item.component.css'],
})
export class FilmLocationItemComponent implements OnInit {
  @Input() filmLocation!: FilmLocation;

  constructor() {}

  ngOnInit(): void {}
}
