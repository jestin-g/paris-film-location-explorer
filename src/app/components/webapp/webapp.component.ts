import { Component, OnInit, Input } from '@angular/core';
import { FilmLocation } from 'src/app/models/FilmLocation';
import { OdpResponseFormat } from 'src/app/models/OdpResponseFormat';
import { FilmLocationService } from 'src/app/services/film-location.service';

@Component({
  selector: 'app-webapp',
  templateUrl: './webapp.component.html',
  styleUrls: ['./webapp.component.css'],
})
export class WebappComponent implements OnInit {
  filmLocations?: FilmLocation[];
  locationsDisplayed?: FilmLocation[];
  @Input() searchText: String = '';

  constructor(private filmLocationService: FilmLocationService) {}

  ngOnInit(): void {
    this.filmLocationService.getFilmLocations().subscribe((response) => {
      let results = response as OdpResponseFormat;
      this.filmLocations = results.records as FilmLocation[];
      this.locationsDisplayed = this.filmLocations;
    });
  }

  filterDisplayedLocations(): void {
    this.locationsDisplayed = [];
    if (this.searchText === '') {
      this.locationsDisplayed = this.filmLocations;
    }
    this.locationsDisplayed = this.filmLocations?.filter((location) =>
      location.fields.nom_tournage
        .toLowerCase()
        .includes(this.searchText.toLowerCase())
    );
  }
}
