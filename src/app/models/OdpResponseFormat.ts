import { FilmLocation } from './FilmLocation';

export interface OdpResponseFormat {
  nhits: number;
  parameters: {
    dataset: string;
    rows: number;
    start: number;
    sort: String[];
    format: String;
    timezone: String;
  };
  records: FilmLocation[];
}
