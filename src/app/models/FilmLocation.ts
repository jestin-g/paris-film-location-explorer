export interface FilmLocation {
  datasetid: String;
  recordid: String;
  fields: {
    coord_y: number;
    type_tournage: String;
    nom_producteur: String;
    date_fin: String;
    geo_point_2d: [number, number];
    nom_tournage: String;
    ardt_lieu: String;
    geo_shape: {
      coordinates: [number, number];
      type: String;
    };
    id_lieu: String;
    nom_realisateur: String;
    adresse_lieu: String;
    date_debut: String;
    annee_tournage: String;
    coord_x: number;
  };
  geometry: {
    type: String;
    coordinates: [number, number];
  };
  record_timestamp: String;
}
